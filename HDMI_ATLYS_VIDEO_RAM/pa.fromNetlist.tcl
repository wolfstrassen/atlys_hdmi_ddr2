
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name HDMI_ATLYS_VIDEO_DECODER -dir "C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/planAhead_run_1" -part xc6slx45csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER} {ipcore_dir} }
add_files [list {C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/Sync_Latency.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/BLM_MEM_VIDEO.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/Write_buffer.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "main.ucf" [current_fileset -constrset]
add_files [list {main.ucf}] -fileset [get_property constrset [current_run]]
open_netlist_design
