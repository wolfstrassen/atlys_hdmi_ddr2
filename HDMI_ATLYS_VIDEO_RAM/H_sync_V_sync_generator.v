`timescale 1ns / 1ps

module H_sync_V_sync_generator(
		input  wire [10:0] tc_hsblnk, // Parametros dependen de la resolucion de salida
		input  wire [10:0] tc_hssync,
		input  wire [10:0] tc_hesync,
		input  wire [10:0] tc_heblnk,

		output wire [10:0] hcount,
		output wire        hsync,
		output wire        hblnk,

		input  wire [10:0] tc_vsblnk,
		input  wire [10:0] tc_vssync,
		input  wire [10:0] tc_vesync,
		input  wire [10:0] tc_veblnk,

		output wire [10:0] vcount,
		output wire        vsync,
		output wire        vblnk,

		input  wire        restart,
		input  wire        clk
    );

  reg    [10:0] hpos_cnt = 0;
  wire          hpos_clr;
  
  reg    [10:0] vpos_cnt = 0;
  wire          vpos_clr;
  wire          vpos_ena;
 
  assign hpos_clr = ((hpos_cnt >= tc_heblnk) ) || restart; 
  assign vpos_ena = hpos_clr;
  assign vpos_clr = ((vpos_cnt >= tc_veblnk) && vpos_ena ) || restart;
  
  assign hcount = hpos_cnt;
  assign hblnk	 = (hcount > tc_hsblnk);
  assign hsync  = (hcount > tc_hssync) && (hcount <= tc_hesync);
  
  assign vcount = vpos_cnt;
  assign vblnk  = (vcount > tc_vsblnk);
  assign vsync  = (vcount > tc_vssync) && (vcount <= tc_vesync);
  
  
  always @(posedge clk)
  begin : hcounter
    if (hpos_clr) hpos_cnt <= 11'b000_0000_0000;
    else hpos_cnt <= hpos_cnt + 11'd1;
  end
  
  always @(posedge clk)
  begin : vcounter
    if (vpos_clr) vpos_cnt <= 11'b000_0000_0000;
    else if (vpos_ena) vpos_cnt <= vpos_cnt + 11'd1;
  end
       
endmodule
