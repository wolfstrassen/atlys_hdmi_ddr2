`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:10:37 08/16/2012 
// Design Name: 
// Module Name:    dispatcher_out_new 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dispatcher_out_new #
	(
		parameter MICRO_TOP = 64,
		parameter MACRO_TOP = 320,
		parameter RAM_ADDR_BITS = 10,
		parameter DDR_PORT_BITS = 64
	)
	( 
		input clk,
		input os_start,
		input [29:0] initial_ddr2_add,
		output reg [RAM_ADDR_BITS-1:0] buff_add,
		input [DDR_PORT_BITS-1:0] data_in,
		output reg [DDR_PORT_BITS-1:0] data_out,
		input calib_done,
		input full,
		output reg pn_wr_en,
		output reg [2:0] pn_cmd_instr,
		output reg [5:0] pn_cmd_bl,
		output reg pn_cmd_en,
		output reg [29:0] pn_cmd_byte_addr
    );

reg [1:0] state = 0;
reg [16:0] data_count=0;
reg nos_start = 1;
reg [6:0] micro_count = 0;
reg first_burst = 1;

initial pn_wr_en = 0;
initial pn_cmd_bl = 0;
initial pn_cmd_en = 0;
initial pn_cmd_byte_addr = 0;

always @(posedge clk) begin
	if (pn_cmd_en) pn_cmd_en <= 0;
	nos_start <= os_start;
	case (state)
		2'd0: begin
					if (calib_done) begin
						first_burst <= 1;
						buff_add <= 0;
						pn_wr_en <= 0;
						data_count <=0;
						if (os_start && ~nos_start) state <= 2'd1;	
					end
				end
		2'd1: begin
					data_out <= data_in;
					buff_add <= data_count;
					if(~full) begin
						data_count<= data_count + 1;
						micro_count <= micro_count + 1;
						pn_wr_en <= 1;
						if (MICRO_TOP-1 == micro_count) 	state <= 2;
					end
					else pn_wr_en <= 0;
					if (MICRO_TOP == micro_count) 	state <= 2;
				end
		2'd2: begin
					pn_wr_en <= 0;
					micro_count <= 0;
					pn_cmd_instr <= 3'b000;  
					pn_cmd_bl <= MICRO_TOP-1;	
					if (first_burst ) begin
						pn_cmd_byte_addr <= initial_ddr2_add;
						first_burst <= 0;
					end
					else pn_cmd_byte_addr <= pn_cmd_byte_addr + 10'd512;
					
					if (data_count == MACRO_TOP) state <= 0;
					else state <= 1;				
					pn_cmd_en <= 1;
				end
	
	endcase
end

endmodule
