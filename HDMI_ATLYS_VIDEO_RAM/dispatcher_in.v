`timescale 1ns / 1ps

module dispatcher_in#
	(
		parameter MICRO_TOP = 64,
		parameter MACRO_TOP = 320,
		parameter RAM_ADDR_BITS = 10,
		parameter DDR_PORT_BITS = 64
	)
	(
		input clk,
		input os_start,
		input [29:0] initial_ddr2_add,
		output reg [RAM_ADDR_BITS-1:0] wr_buff_add,
		output we_buff,
		input [DDR_PORT_BITS-1:0] data_in,
		output [DDR_PORT_BITS-1:0] data_out,
		input calib_done,
		input empty,
		output wire pn_rd_en,
		output reg [2:0] pn_cmd_instr,
		output reg [5:0] pn_cmd_bl,
		output reg pn_cmd_en,
		output reg [29:0] pn_cmd_byte_addr
	);

	assign pn_rd_en = pn_rd_en_state & (~empty);
	assign data_out = data_in;
	assign we_buff = pn_rd_en;
	
	reg [1:0] state;
	reg pn_rd_en_state;
	reg [RAM_ADDR_BITS-1:0] buff_add;
	reg [6:0] micro_count; // tiene que llegar a 64 (con 5 bits solo llega a 63)
	reg [16:0] macro_count; /*cambiado*/
	reg [16:0] n_data_rd; /*cambiado*/
	reg lock;

	initial state = 0;
	initial pn_cmd_en = 0;
	initial buff_add = 0;
	initial micro_count = 0;
	initial macro_count = 0;
	initial n_data_rd = 0;
	initial pn_rd_en_state = 0;
	initial lock = 0;
	reg os_start_temp = 0;
	reg first_burst = 1;
	always@(posedge clk) begin
		wr_buff_add <= buff_add;
		os_start_temp <= os_start;
		//--- state 0
		if(calib_done && state == 0) begin
			pn_rd_en_state <= 0;
			pn_cmd_en <= 0;
			if(os_start && ~os_start_temp) begin
				first_burst <=1;
				state <= 1;
				lock <= 0;
				buff_add <= 0;
				pn_cmd_byte_addr <= initial_ddr2_add;
				macro_count <= 0;
				n_data_rd <= MACRO_TOP;
			end
		end
		//--- state 1
		if(calib_done && state == 1) begin
			if(n_data_rd > MICRO_TOP) begin
				pn_cmd_bl <= MICRO_TOP-1;
				n_data_rd <= n_data_rd-64;
			end
			else
				pn_cmd_bl <= n_data_rd-1;
		
			lock <= 0;
			micro_count <= 0;
			state <= 2;
			if (first_burst) first_burst <= 0;
			else pn_cmd_byte_addr <= pn_cmd_byte_addr + 10'd512; // incrementos en direcciones de 8 bytes
			pn_cmd_instr <= 3'b001;           // comando de escritura
			pn_cmd_en <= 1;
		end
		//--- state 2
		if(calib_done && state == 2) begin
			pn_cmd_en <= 0;
			if(~empty) begin
				pn_rd_en_state <= 1;
				if (macro_count != 0) buff_add <= buff_add+1;
				micro_count <= micro_count+1;
				macro_count <= macro_count+1;
				lock <= 1;
			end
			else begin
				pn_rd_en_state <= 0;
				if(micro_count == MICRO_TOP +1)
					state <= 1;
				if(macro_count == MACRO_TOP +1)
					state <= 0;
				if(lock) begin
				   if (buff_add != 0) buff_add <= buff_add-1;
					if (micro_count != 0) micro_count <= micro_count-1;
					if (macro_count != 0) macro_count <= macro_count-1;
					lock <= 0;
				end
			end
		end
		if(state == 3)
			state <= 0;
	end
endmodule
