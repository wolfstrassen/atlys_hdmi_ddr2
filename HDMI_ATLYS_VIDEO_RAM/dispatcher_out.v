`timescale 1ns / 1ps
module dispatcher_out #
	(
		parameter MICRO_TOP = 64,
		parameter MACRO_TOP = 320,
		parameter RAM_ADDR_BITS = 10,
		parameter DDR_PORT_BITS = 64
	)
	( 
		input clk,
		input os_start,
		input [29:0] initial_ddr2_add,
		output reg [RAM_ADDR_BITS-1:0] buff_add,
		input [DDR_PORT_BITS-1:0] data_in,
		output [DDR_PORT_BITS-1:0] data_out,
		input calib_done,
		input full,
		output wire pn_wr_en,
		output reg [2:0] pn_cmd_instr,
		output reg [5:0] pn_cmd_bl,
		output reg pn_cmd_en,
		output reg [29:0] pn_cmd_byte_addr
    );

	reg pn_wr_en_state;
	reg [6:0] micro_count; // tiene que llegar a 64 (con 5 bits solo llega a 63)
	reg [16:0] macro_count; /*cambiado*/
	reg lock;
	reg top;
	reg [1:0] state;
	
	assign pn_wr_en = (~full) & pn_wr_en_state;
	assign data_out = data_in;
	
	initial state = 0;
	initial pn_cmd_en = 0;
	initial buff_add = 0;
	initial micro_count = 0;
	initial macro_count = 0;
	initial pn_wr_en_state = 0;
	initial lock = 0;
	initial top = 0;
	reg os_start_past = 0;
	reg first_burst = 1;
	always@(posedge clk) begin
		//--- state 0
		if(calib_done && state == 0) begin
			pn_cmd_en <= 0;
			buff_add <= 0;
			micro_count <= 0;
			macro_count <= 0;
			pn_wr_en_state <= 0;
			lock <= 0; // asi no entra a add-1 si es que parte y esta full
			top <= 0;
			os_start_past <= os_start;
			if(os_start && ~os_start_past) begin
				pn_cmd_byte_addr <= initial_ddr2_add;
				state<=1;
				first_burst <= 1;
			end
		end
		//--- state 1
		if(calib_done && state == 1) begin
			pn_cmd_en <= 0;

			if(~full) begin
				if(macro_count == MACRO_TOP) begin // siempre llega este antes que el MACRO_TOP+1
					top <= 1;                    // no es necesario ponerlo en el full = 1 (full se atiende un ciclo retardado)
					pn_wr_en_state <= 0;
					state <= 2;
				end
				else begin
					pn_wr_en_state <= 1;
					micro_count <= micro_count+1;
					macro_count <= macro_count+1;
					buff_add <= buff_add+1;
					lock <= 1;
				end
			end
			else begin
				pn_wr_en_state <= 0;
				if(micro_count == (MICRO_TOP+1))
					state <= 2;
				if(lock) begin
					buff_add <= buff_add-1;
					micro_count <= micro_count-1;
					macro_count <= macro_count-1;
					lock <= 0;
				end
			end
		end
		//--- state 2
		if (calib_done && state == 2) begin
			lock <= 0; // no debería ser necesario
			micro_count <= 0;
			if(top)
				state <= 0;
			else
				state <= 1;
			
			pn_cmd_instr <= 3'b000;           // comando de escritura
			pn_cmd_bl <= micro_count-1;;      // numero de palabras (0 es considerado como 1 dato)
			if (first_burst) first_burst <= 0;
			else pn_cmd_byte_addr <= pn_cmd_byte_addr + 10'd512; // incrementos en direcciones de 8 bytes
			pn_cmd_en <= 1;
		end
		if(state == 3)
			state <= 0;
	end

endmodule
