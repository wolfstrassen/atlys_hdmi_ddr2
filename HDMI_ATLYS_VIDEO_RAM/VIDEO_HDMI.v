`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

//****************************************************************
//PARA EL PUERTO HDMI A DE LA TARJETA NEXYS 3
//CONDIGURADO PARA SALIDA SVGA
//NET "TMDS(0)"  	LOC = C7   |IOSTANDARD = TMDS_33 ; 
//NET "TMDSB(0)"  LOC = A7   |IOSTANDARD = TMDS_33 ;
//NET "TMDS(1)"  	LOC = B6   |IOSTANDARD = TMDS_33 ; 
//NET "TMDSB(1)"  LOC = A6   |IOSTANDARD = TMDS_33 ;
//NET "TMDS(2)"  	LOC = C5   |IOSTANDARD = TMDS_33 ; 
//NET "TMDSB(2)"  LOC = A5   |IOSTANDARD = TMDS_33 ;
//NET "TMDS(3)"  	LOC = D11  |IOSTANDARD = TMDS_33 ; # Clock
//NET "TMDSB(3)"  LOC = C11  |IOSTANDARD = TMDS_33 ;
//****************************************************************

//****************************************************************
//CONSTRAINS DE TIEMPO:

//NET "pclk" TNM_NET = "TNM_PCLK";
//TIMESPEC "TS_PCLK" = PERIOD "TNM_PCLK" 40 MHz HIGH 50 % PRIORITY 0 ;


//NET "pclkx2" TNM_NET = "TNM_PCLKX2";
//TIMESPEC "TS_PCLKX2" = PERIOD "TNM_PCLKX2" TS_PCLK * 2;

//NET "pclkx10" TNM_NET = "TNM_PCLKX10";
//TIMESPEC "TS_PCLKX10" = PERIOD "TNM_PCLKX10" TS_PCLK * 10;

//TIMEGRP "bramgrp" = RAMS(enc1/pixel2x/dataint<*>);  
//TIMEGRP "fddbgrp" = FFS(enc1/pixel2x/db<*>);
//TIMEGRP "bramra" = FFS(enc1/pixel2x/ra<*>);

//TIMESPEC "TS_ramdo" = FROM "bramgrp" TO "fddbgrp" TS_PCLK;
//TIMESPEC "TS_ramra" = FROM "bramra" TO "fddbgrp" TS_PCLK;

//****************************************************************


//////////////////////////////////////////////////////////////////////////////////
module VIDEO_HDMI # (
		parameter [10:0] HPIXELS = 11'd720,
		parameter [10:0] VLINES = 11'd480,
		parameter [10:0] HSYNCPW = 11'd40,
		parameter [10:0] VSYNCPW = 11'd3,
		parameter [10:0] HFNPRCH = 11'd24,
		parameter [10:0] VFNPRCH = 11'd10,
		parameter [10:0] HBKPRCH = 11'd96,
		parameter [10:0] VBKPRCH = 11'd32,
		parameter hvsync_polarity = 1'b0,
		parameter CLK_FAST = 1
	)
	(
	input wire clk, //Reloj a la velocidad del pixel clock.
	input wire [7:0] red_data,
	input wire [7:0] green_data,
	input wire [7:0] blue_data,
	output wire [3:0] TMDS,
   output wire [3:0] TMDSB,
	output wire [10:0] hcount,
	output wire [10:0] vcount,
	output wire active,
	input wire reset,
	input [5:0] latencia
   );


	
	wire pclk,pclkx2,pclkx10;
	wire serdesstrobe;
	
	create_clocks #(
			.CLK_FAST(CLK_FAST) // si Pixel Clock * 10 < 400 MHz, pon un 0, sino un 1
		)
	clker(	.clk(clk),
				.pclkx1(pclk),
				.pclkx2(pclkx2),
				.pclkx10(pclkx10),
				.serdesstrobe(serdesstrobe)
				);
	

  wire [10:0] tc_hsblnk;
  wire [10:0] tc_hssync;
  wire [10:0] tc_hesync;
  wire [10:0] tc_heblnk;
  wire [10:0] tc_vsblnk;
  wire [10:0] tc_vssync;
  wire [10:0] tc_vesync;
  wire [10:0] tc_veblnk;
  
  assign tc_hsblnk = HPIXELS - 11'd1;
  assign tc_hssync = HPIXELS - 11'd1 + HFNPRCH;
  assign tc_hesync = HPIXELS - 11'd1 + HFNPRCH + HSYNCPW;
  assign tc_heblnk = HPIXELS - 11'd1 + HFNPRCH + HSYNCPW + HBKPRCH;
  assign tc_vsblnk =  VLINES - 11'd1;
  assign tc_vssync =  VLINES - 11'd1 + VFNPRCH;
  assign tc_vesync =  VLINES - 11'd1 + VFNPRCH + VSYNCPW;
  assign tc_veblnk =  VLINES - 11'd1 + VFNPRCH + VSYNCPW + VBKPRCH;
  
  
  wire VGA_HSYNC_INT, VGA_VSYNC_INT;
  wire          bgnd_hsync;
  wire          bgnd_hblnk;
  wire          bgnd_vsync;
  wire          bgnd_vblnk;


  
  H_sync_V_sync_generator hv(
    .tc_hsblnk(tc_hsblnk), //input
    .tc_hssync(tc_hssync), //input
    .tc_hesync(tc_hesync), //input
    .tc_heblnk(tc_heblnk), //input
    .hcount(hcount), //output
    .hsync(VGA_HSYNC_INT), //output
    .hblnk(bgnd_hblnk), //output
    .tc_vsblnk(tc_vsblnk), //input
    .tc_vssync(tc_vssync), //input
    .tc_vesync(tc_vesync), //input
    .tc_veblnk(tc_veblnk), //input
    .vcount(vcount), //output
    .vsync(VGA_VSYNC_INT), //output
    .vblnk(bgnd_vblnk), //output
    .restart(reset),
    .clk(pclk)
	 );
  
  assign active = !bgnd_hblnk && !bgnd_vblnk;
  reg active_q;
  reg vsync, hsync;
  wire VGA_HSYNC, VGA_VSYNC;
  wire de;
  
  
  
  Sync_Latency latencia_fix (.clk(pclk),
								 .q({VGA_HSYNC,VGA_VSYNC,de}),
								 .d({hsync,vsync,active_q}),
								 .a(latencia));

  always @ (posedge pclk) 
  begin
    hsync <= VGA_HSYNC_INT ^ hvsync_polarity ;
    vsync <= VGA_VSYNC_INT ^ hvsync_polarity ;
	 active_q <= active;
  end
   
 
	dvi_encoder_top enc1(.pclk(pclk),
								.pclkx2(pclkx2),
								.pclkx10(pclkx10),
								.rstin(reset),
								.blue_din(blue_data),
								.green_din(green_data),
								.red_din(red_data),
								.hsync(VGA_HSYNC),
								.vsync(VGA_VSYNC),
								.de(de),
								.serdesstrobe(serdesstrobe),
								.TMDS(TMDS),
								.TMDSB(TMDSB));

 
endmodule
	