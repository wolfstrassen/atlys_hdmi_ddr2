`default_nettype none
`timescale 1ns / 1ps

module top(
		input wire clk,
		output wire [3:0] TMDS,
		output wire [3:0] TMDSB,
		output reg LED,
				
		//--- Interfaz con DDR2
		output wire DDR2CLK_P,
		output wire DDR2CLK_N,
		output wire DDR2CKE,
		output wire DDR2RASN,
		output wire DDR2CASN,
		output wire DDR2WEN,
		inout wire DDR2RZQ,
		inout wire DDR2ZIO,
		output wire [2:0] DDR2BA,

		output wire [12:0] DDR2A,
		inout wire [15:0] DDR2DQ,

		inout wire DDR2UDQS_P,
		inout wire DDR2UDQS_N,
		inout wire DDR2LDQS_P,
		inout wire DDR2LDQS_N,
		output wire DDR2LDM,
		output wire DDR2UDM,
		output wire DDR2ODT,	
		input wire RxD
		 );
		 
parameter [7:0] cabecera1=8'd255;	
parameter [7:0] cabecera2=8'd32;	
parameter himres = 640;
parameter vimres = 480;
parameter himres_x4 = himres*4;
parameter n_frames = 1;
							
/*INTERFAZ DDR2*/

	wire [2:0] c3_p0_cmd_instr;
	wire [5:0] c3_p0_cmd_bl;
	wire [29:0] c3_p0_cmd_byte_addr;
	wire [7:0] c3_p0_wr_mask;
	wire [63:0] c3_p0_wr_data; 
	wire [2:0] c3_p1_cmd_instr;
	wire [5:0] c3_p1_cmd_bl;
	wire [29:0] c3_p1_cmd_byte_addr;
	wire [7:0] c3_p1_wr_mask;
	wire [63:0] c3_p1_wr_data;
	wire c3_p0_wr_en; 
	wire c3_p1_wr_en;
	wire c3_p0_rd_en;
	wire c3_p1_rd_en;
	wire c3_p0_cmd_en;
	wire c3_p1_cmd_en;
	wire reset;

	wire [6:0] c3_p0_wr_count;
	wire [6:0] c3_p1_wr_count;
	wire [63:0] c3_p0_rd_data;
	wire [63:0] c3_p1_rd_data;
	wire [6:0] c3_p0_rd_count;
	wire [6:0] c3_p1_rd_count;
	wire c3_p0_wr_full;
	wire c3_p0_wr_empty;
	wire c3_p1_wr_full;
	wire c3_p1_wr_empty;
	wire c3_p0_rd_empty, c3_p1_rd_empty;
	wire c3_calib_done;
	wire c3_clk0,sys_CLK;
	
	assign c3_p0_wr_mask = 0;
	assign c3_p1_wr_mask = 0;
	assign c3_p1_wr_en = 0;
	assign c3_p0_rd_en = 0;
	assign reset = 0;


//GENERADOR DE RELOJ DE PIXEL//
wire pclk,pclk_lckd,mem_clk;

Px_clk Pixel_clk_gen
   (.clk(clk),      // IN
    .mem_clk(mem_clk),
    .pclk(pclk),
    .LOCKED(pclk_lckd)
	 );

ddr2_user_interface DDR2_MCB 
	(
		.DDR2CLK_P(DDR2CLK_P), 
		.DDR2CLK_N(DDR2CLK_N), 
		.DDR2CKE(DDR2CKE), 
		.DDR2RASN(DDR2RASN), 
		.DDR2CASN(DDR2CASN), 
		.DDR2WEN(DDR2WEN), 
		.DDR2RZQ(DDR2RZQ), 
		.DDR2ZIO(DDR2ZIO), 
		.DDR2BA(DDR2BA), 
		.DDR2A(DDR2A), 
		.DDR2DQ(DDR2DQ), 
		.DDR2UDQS_P(DDR2UDQS_P), 
		.DDR2UDQS_N(DDR2UDQS_N), 
		.DDR2LDQS_P(DDR2LDQS_P), 
		.DDR2LDQS_N(DDR2LDQS_N), 
		.DDR2LDM(DDR2LDM), 
		.DDR2UDM(DDR2UDM), 
		.DDR2ODT(DDR2ODT), 
		.clk(mem_clk), 
		.c3_p0_cmd_en(c3_p0_cmd_en), 
		.c3_p0_cmd_instr(c3_p0_cmd_instr), 
		.c3_p0_cmd_bl(c3_p0_cmd_bl), 
		.c3_p0_cmd_byte_addr(c3_p0_cmd_byte_addr), 
		.c3_p0_wr_mask(c3_p0_wr_mask), 
		.c3_p0_wr_data(c3_p0_wr_data), 
		.c3_p0_wr_full(c3_p0_wr_full), 
		.c3_p0_wr_empty(c3_p0_wr_empty), 
		.c3_p0_wr_count(c3_p0_wr_count), 
		.c3_p0_rd_data(c3_p0_rd_data), 
		.c3_p0_rd_count(c3_p0_rd_count), 
		.c3_p0_rd_en(c3_p0_rd_en), 
		.c3_p0_rd_empty(c3_p0_rd_empty), 
		.c3_p0_wr_en(c3_p0_wr_en), 
		.c3_p1_cmd_en(c3_p1_cmd_en), 
		.c3_p1_cmd_instr(c3_p1_cmd_instr), 
		.c3_p1_cmd_bl(c3_p1_cmd_bl), 
		.c3_p1_cmd_byte_addr(c3_p1_cmd_byte_addr), 
		.c3_p1_wr_mask(c3_p1_wr_mask), 
		.c3_p1_wr_full(c3_p1_wr_full), 
		.c3_p1_wr_empty(c3_p1_wr_empty), 
		.c3_p1_wr_count(c3_p1_wr_count), 
		.c3_p1_wr_data(c3_p1_wr_data), 
		.c3_p1_rd_data(c3_p1_rd_data), 
		.c3_p1_rd_count(c3_p1_rd_count), 
		.c3_p1_rd_en(c3_p1_rd_en), 
		.c3_p1_rd_empty(c3_p1_rd_empty), 
		.c3_p1_wr_en(c3_p1_wr_en), 
		.c3_calib_done(c3_calib_done), 
		.reset(reset),
		.c3_clk0(c3_clk0)
	);



	
	// ---- CACHE
	wire [9:0] wr_buff_add;
	wire [63:0] data_in_cache_in;
	wire wr_en;

	wire [10:0] hpos;
	wire [31:0] Data_OUT_RGB;
	reg [29:0] init_add_rd=0;
	reg [29:0] init_add_wr=0;
	reg os_start_wr = 0;
	reg os_start_rd = 0;
	reg [10:0] address_display = 0;
	
	BLM_MEM_VIDEO Read_Cache (
		.clka(c3_clk0), // input clka
		.wea(wr_en), // input [0 : 0] wea
		.addra(wr_buff_add), // input [9 : 0] addra
		.dina(data_in_cache_in), // input [63 : 0] dina
		.clkb(pclk), // input clkb
		.addrb(hpos), // input [10 : 0] addrb
		.doutb(Data_OUT_RGB) // output [31 : 0] doutb
	);
	
	
	dispatcher_in #
	(
		.MICRO_TOP(64),
		.MACRO_TOP(himres/2),
		.RAM_ADDR_BITS(10),
		.DDR_PORT_BITS(64)
	) 
	dispatcher_in_unit 
	(
		.clk(c3_clk0), 
		.os_start(os_start_rd), 
		.initial_ddr2_add(init_add_rd ), 
		.wr_buff_add(wr_buff_add), 
		.we_buff(wr_en), 
		.data_in(c3_p1_rd_data), 
		.data_out(data_in_cache_in), 
		.calib_done(c3_calib_done), 
		.empty(c3_p1_rd_empty), 
		.pn_rd_en(c3_p1_rd_en), 
		.pn_cmd_instr(c3_p1_cmd_instr), 
		.pn_cmd_bl(c3_p1_cmd_bl), 
		.pn_cmd_en(c3_p1_cmd_en), 
		.pn_cmd_byte_addr(c3_p1_cmd_byte_addr)
	);

	
	wire [63:0] data_in_disp;
	wire [9:0] buff_add;
	reg [12:0] write_buffer_address = 13'd0;
	reg we_cache_O = 1'b0;
	reg [7:0] data_to_write = 8'd0;
	
	Write_buffer Write_Cache (
	  .clka(c3_clk0), // input clka
	  .wea(we_cache_O), // input [0 : 0] wea
	  .addra(write_buffer_address), // input [12 : 0] addra
	  .dina(data_to_write), // input [7 : 0] dina
	  .clkb(c3_clk0), // input clkb
	  .addrb(buff_add), // input [9 : 0] addrb
	  .doutb(data_in_disp) // output [63 : 0] doutb
	);
	
	
	
	dispatcher_out #
	(
		.MICRO_TOP(64),
		.MACRO_TOP(himres/2),
		.RAM_ADDR_BITS(10),
		.DDR_PORT_BITS(64)
	)
	dispatcher_out_unit 
	(
		.clk(c3_clk0),  
		.os_start(os_start_wr), 
		.initial_ddr2_add(init_add_wr), 
		.buff_add(buff_add), 
		.data_in(data_in_disp), 
		.data_out(c3_p0_wr_data), 
		.calib_done(c3_calib_done), 
		.full(c3_p0_wr_full), 
		.pn_wr_en(c3_p0_wr_en), 
		.pn_cmd_instr(c3_p0_cmd_instr), 
		.pn_cmd_bl(c3_p0_cmd_bl), 
		.pn_cmd_en(c3_p0_cmd_en), 
		.pn_cmd_byte_addr(c3_p0_cmd_byte_addr)
	);







//MODULO DE VIDEO HDMI//

wire ReadyRX,TxD_Busy;


wire [7:0] GPout;
reg [7:0] red_data = 0, blue_data = 0, green_data= 0;

serial Modulo_serial(.clk(c3_clk0),.RxD(RxD),.GPout(GPout),
					  .new_data(ReadyRX));

wire [10:0] vpos;
wire active_video;
assign active_video = (hpos < himres) && (vpos < vimres);
//Resolucion VGA 640x480 @60 Hz, BW: 25 MHz
VIDEO_HDMI #(
			.HPIXELS(640), //Resolucion horizontal 
			.VLINES(480),   //Resolucion vertical
			.HFNPRCH(32),	 //Horizontal Front Porch
			.HSYNCPW(88),	 //Horizontal sync pulse width
			.HBKPRCH(32),   //Horizontal Back porch
			.VFNPRCH(10),   //Vertical Front Porch
			.VSYNCPW(5),    //Vertical sync pulse width  
			.VBKPRCH(10),   //Vertical Back porch
			.hvsync_polarity(1),
			.CLK_FAST(0)	 //Si el reloj de pixel es mas bajo que 40 MHz es un 0, sino un 1
		)
		HDMI_A ( 	  .clk(pclk),					
						  .red_data(active_video? Data_OUT_RGB[23:16]: 8'd0),   //COMPONENTE R (8 bits)
						  .green_data(active_video? Data_OUT_RGB[15:8]: 8'd0),//COMPONENTE G (8 bits)
						  .blue_data(active_video? Data_OUT_RGB[7:0]: 8'd0), //COMPONENTE B (8 bits)
						  .TMDS(TMDS),			 //SALIDA AL VIDEO HDMI
						  .TMDSB(TMDSB),      //SALIDA COMPLEMENTARIA AL VIDEO HDMI
						  .hcount(hpos),      //PIXEL HORIZONTAL A DESPLEGAR
						  .vcount(vpos),      //PIXEL VERTICAL A DESPLEGAR
						  .latencia(5'd0), //Compensa latencia por procesamiento o registros
						  .reset(~pclk_lckd)
				);
////////////////////////////////////////////						  

reg [3:0] state = 0;
reg [13:0] counter = 0; 
reg [11:0] frame_counter = 0;

always @(posedge pclk) begin
	if (os_start_rd) os_start_rd <= 0;	
	
	if ((vpos == vimres) && (hpos == himres)) begin
		os_start_rd <= 1;
		if (frame_counter >= (n_frames-1)) begin 
			frame_counter <=0;
			init_add_rd <= 0;
		end
		else frame_counter <= frame_counter +1;
	end
	
	if ((vpos < vimres) && (hpos == himres)) begin
		init_add_rd <= init_add_rd + himres_x4;
		os_start_rd <= 1;
	end

end


always @(posedge c3_clk0) begin
	if (os_start_wr == 1) os_start_wr <= 0;	
	if (we_cache_O == 1) we_cache_O <= 0;
	
	if (ReadyRX) begin
		(* PARALLEL_CASE *)	
		case (state)
		4'd0:	if (GPout == cabecera1) state <=1;
		4'd1: begin
					if (GPout == cabecera2)	state <=2;	
					else state <= 0;
				end
		4'd2: begin
					if (GPout == 255) init_add_wr <= init_add_wr + himres_x4;
					if (GPout == 253) init_add_wr <= init_add_wr - himres_x4;
					if (GPout == 100) init_add_wr <=  0;
					LED <= 1;
					state <= 3;
				end
		4'd3: begin
					write_buffer_address <= counter;
					data_to_write <= GPout;
					we_cache_O <= 1;
					if (counter == (himres_x4-1)) begin
							counter <=0;
							state <= 0;
							LED <= 0;
							os_start_wr <= 1;					
					end 
					else counter <= counter +1; 
				end
		endcase
	end  


end
					  

endmodule
