////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: O.87xd
//  \   \         Application: netgen
//  /   /         Filename: Sync_Latency.v
// /___/   /\     Timestamp: Wed Aug 08 15:15:30 2012
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog "C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/tmp/_cg/Sync_Latency.ngc" "C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/tmp/_cg/Sync_Latency.v" 
// Device	: 6slx16csg324-3
// Input file	: C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/tmp/_cg/Sync_Latency.ngc
// Output file	: C:/Users/Alejandro Wolf/Documents/Udec/2012-2/HDMI_ATLYS/HDMI_ATLYS_VIDEO_DECODER/tmp/_cg/Sync_Latency.v
// # of Modules	: 1
// Design Name	: Sync_Latency
// Xilinx        : C:\Xilinx\13.4\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Sync_Latency (
  clk, a, d, q
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input [5 : 0] a;
  input [2 : 0] d;
  output [2 : 0] q;
  
  // synthesis translate_off
  
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_2_1_25 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_2_01 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_2_0_23 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_1_1_22 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_1_01 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_1_0_20 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_0_1_19 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_0_01 ;
  wire \U0/i_synth/i_bb_inst/Mshreg_output_net_0_0_17 ;
  wire \U0/i_synth/i_bb_inst/ce ;
  wire \NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_2_1_Q31_UNCONNECTED ;
  wire \NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_1_1_Q31_UNCONNECTED ;
  wire \NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_0_1_Q31_UNCONNECTED ;
  wire [2 : 0] \U0/i_synth/i_bb_inst/output_net ;
  FD #(
    .INIT ( 1'b0 ))
  \U0/i_synth/i_bb_inst/gen_output_regs.output_regs/fd/output_1  (
    .C(clk),
    .D(\U0/i_synth/i_bb_inst/output_net [0]),
    .Q(q[0])
  );
  FD #(
    .INIT ( 1'b0 ))
  \U0/i_synth/i_bb_inst/gen_output_regs.output_regs/fd/output_2  (
    .C(clk),
    .D(\U0/i_synth/i_bb_inst/output_net [1]),
    .Q(q[1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \U0/i_synth/i_bb_inst/gen_output_regs.output_regs/fd/output_3  (
    .C(clk),
    .D(\U0/i_synth/i_bb_inst/output_net [2]),
    .Q(q[2])
  );
  MUXF7   \U0/i_synth/i_bb_inst/output_net_2_f7  (
    .I0(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_01 ),
    .I1(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_1_25 ),
    .S(a[5]),
    .O(\U0/i_synth/i_bb_inst/output_net [2])
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_2_1  (
    .CLK(clk),
    .D(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_0_23 ),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_1_25 ),
    .Q31(\NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_2_1_Q31_UNCONNECTED ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_2_0  (
    .CLK(clk),
    .D(d[2]),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_01 ),
    .Q31(\U0/i_synth/i_bb_inst/Mshreg_output_net_2_0_23 ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  MUXF7   \U0/i_synth/i_bb_inst/output_net_1_f7  (
    .I0(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_01 ),
    .I1(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_1_22 ),
    .S(a[5]),
    .O(\U0/i_synth/i_bb_inst/output_net [1])
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_1_1  (
    .CLK(clk),
    .D(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_0_20 ),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_1_22 ),
    .Q31(\NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_1_1_Q31_UNCONNECTED ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_1_0  (
    .CLK(clk),
    .D(d[1]),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_01 ),
    .Q31(\U0/i_synth/i_bb_inst/Mshreg_output_net_1_0_20 ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  MUXF7   \U0/i_synth/i_bb_inst/output_net_0_f7  (
    .I0(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_01 ),
    .I1(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_1_19 ),
    .S(a[5]),
    .O(\U0/i_synth/i_bb_inst/output_net [0])
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_0_1  (
    .CLK(clk),
    .D(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_0_17 ),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_1_19 ),
    .Q31(\NLW_U0/i_synth/i_bb_inst/Mshreg_output_net_0_1_Q31_UNCONNECTED ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  SRLC32E #(
    .INIT ( 32'h00000000 ))
  \U0/i_synth/i_bb_inst/Mshreg_output_net_0_0  (
    .CLK(clk),
    .D(d[0]),
    .CE(\U0/i_synth/i_bb_inst/ce ),
    .Q(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_01 ),
    .Q31(\U0/i_synth/i_bb_inst/Mshreg_output_net_0_0_17 ),
    .A({a[4], a[3], a[2], a[1], a[0]})
  );
  VCC   \U0/i_synth/i_bb_inst/XST_VCC  (
    .P(\U0/i_synth/i_bb_inst/ce )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
