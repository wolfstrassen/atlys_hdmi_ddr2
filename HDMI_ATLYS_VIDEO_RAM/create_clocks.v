`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:20:34 12/30/2011 
// Design Name: 
// Module Name:    create_clocks 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module create_clocks	# (
		parameter CLK_FAST = 1
	)
	
	(
    input wire clk,
    output wire pclkx1,
    output wire pclkx2,
    output wire pclkx10,
	 output wire serdesstrobe
    );
	  wire plllckd;


	
  PLL_BASE # (
    //.CLKIN_PERIOD(13.75), //Periodo cercano al del reloj de entrada
    .CLKFBOUT_MULT(CLK_FAST? 10: 20), // Clock de salida PLL a 10x
    .CLKOUT0_DIVIDE(CLK_FAST? 1:  2), // Clock de salida a 10x
    .CLKOUT1_DIVIDE(CLK_FAST? 10: 20), //clock de salida a 1x
    .CLKOUT2_DIVIDE(CLK_FAST? 5 : 10), //Clock de salida a 2x
    .COMPENSATION("INTERNAL")
  ) PLL_OSERDES (
    .CLKFBOUT(clkfbout),
    .CLKOUT0(pllclk0),
    .CLKOUT1(pllclk1),
    .CLKOUT2(pllclk2),
    .CLKOUT3(),
    .CLKOUT4(),
    .CLKOUT5(),
    .LOCKED(pll_lckd),
    .CLKFBIN(clkfbout),
    .CLKIN(clk),
    .RST(0)
  );

  
	BUFG	pclkbufg1x (.I(pllclk1), .O(pclkx1));
	BUFG pclkbufg2x (.I(pllclk2), .O(pclkx2));
	
  
	BUFPLL #(.DIVIDE(5)) ioclk_buf (.PLLIN(pllclk0), .GCLK(pclkx2), .LOCKED(pll_lckd),
           .IOCLK(pclkx10), .SERDESSTROBE(serdesstrobe), .LOCK(plllckd));

	
endmodule
