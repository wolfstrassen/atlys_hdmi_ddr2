`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:47:31 05/07/2012 
// Design Name: 
// Module Name:    serial 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module serial(
    input clk,
    input RxD,
    output reg [7:0] GPout,
	 output reg new_data=0
    );

wire [7:0] RxD_data;
async_receiver deserializer(.clk(clk), .RxD(RxD), .RxD_data_ready(RxD_data_ready), 
									 .RxD_data(RxD_data));

always @(posedge clk) begin
	if(RxD_data_ready) begin 
		GPout <= RxD_data ;
		new_data<=1;
	end 
	if (new_data) new_data<=0;
end

endmodule
