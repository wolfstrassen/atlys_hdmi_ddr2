`timescale 1ns / 1ps
module cache_out #
	(
		parameter RAM_WIDTH = 8,
		parameter RAM_ADDR_BITS = 10
	)
	( 
		input clkA,
		input clkB,
		input we,
		input wire [RAM_ADDR_BITS-1:0] add_rd,
		input wire [RAM_ADDR_BITS-1:0] add_wr,
		input wire [RAM_WIDTH-1:0] data_in,
		output reg [RAM_WIDTH-1:0] data_out
	);

	reg [RAM_WIDTH-1:0] mem [(2**RAM_ADDR_BITS)-1:0];

	always @(posedge clkA)
		if (we)
			mem[add_wr] <= data_in;

	always @(posedge clkB)
		data_out <= mem[add_rd];

endmodule
